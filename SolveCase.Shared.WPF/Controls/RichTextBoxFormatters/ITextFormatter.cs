﻿using System;
using System.Windows.Documents;

namespace SolveCase.Shared.WPF.Controls
{
    public interface ITextFormatter
    {
        string GetText(FlowDocument document);
        void SetText(FlowDocument document, string text);
    }
}
