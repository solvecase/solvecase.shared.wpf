﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace SolveCase.Shared.WPF.Converters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class StringEqualsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //return ((string)parameter == (string)value);
            string[] parameters = ((string)parameter).Split(new string[] { "|", "||" }, StringSplitOptions.RemoveEmptyEntries);
            return parameters.Contains((value as string), StringComparer.OrdinalIgnoreCase);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? parameter : null;
        }
    }
}
