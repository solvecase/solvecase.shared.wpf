﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SolveCase.Shared.WPF.Converters
{
    public class ObjectToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {            
            if (null == value || null == parameter)
            {                
                return Visibility.Collapsed;
            }

            return value.Equals(parameter) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == value || null == parameter)
            {
                return Visibility.Collapsed;
            }

            return value.Equals(parameter) ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
