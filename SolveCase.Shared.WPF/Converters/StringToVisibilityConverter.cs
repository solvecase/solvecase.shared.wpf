﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace SolveCase.Shared.WPF.Converters
{
    public class StringToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == parameter || null == value)
            {
                return Visibility.Collapsed;
            }

            if (string.IsNullOrWhiteSpace(parameter as string) || string.IsNullOrEmpty(value as string))
            {
                return Visibility.Collapsed;
            }

            string[] parameters = ((string)parameter).Split(new string[] { "|", "||" }, StringSplitOptions.RemoveEmptyEntries);

            return parameters.Contains((value as string), StringComparer.OrdinalIgnoreCase) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
