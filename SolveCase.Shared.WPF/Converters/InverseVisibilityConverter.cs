﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SolveCase.Shared.WPF.Converters
{
    public class InverseVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return Visibility.Collapsed;
            Visibility visibility = (Visibility)value;
            if (visibility == Visibility.Collapsed)
                return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
