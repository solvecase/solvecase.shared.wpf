﻿using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SolveCase.Shared.WPF.Helpers
{
    public class ImageHelper
    {
        public static byte[] ImageToBytes(ImageSource img)
        {
            byte[] bytes = null;
            var bitmapSource = img as BitmapSource;
            if (bitmapSource != null)
            {
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }
            return bytes;
        }

        public static BitmapImage BytesToImage(byte[] bytes)
        {
            MemoryStream ms = new MemoryStream(bytes);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();
            return bi;
        }
    }
}
