﻿using System.Linq;

namespace SolveCase.Shared.WPF.Helpers
{
    public class ObjectHelper
    {

        public static void CopyProperties<TS, TD>(TS source, TD dest)
        {
            var destPis = typeof(TD).GetProperties().Where(x => x.CanWrite).ToList();
            foreach (var pi in destPis)
            {
                var sPi = typeof(TS).GetProperty(pi.Name);
                if (sPi.CanRead)
                {
                    pi.SetValue(dest, sPi.GetValue(source));
                }
            }
        }
    }
}
